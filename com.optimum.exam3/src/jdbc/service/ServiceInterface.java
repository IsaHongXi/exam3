package jdbc.service;

public interface ServiceInterface {

	void customerChoice();
	void customerAuthentication();
	void closeConnection();
}
