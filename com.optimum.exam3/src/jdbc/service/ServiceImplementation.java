package jdbc.service;

import java.util.Scanner;
import jdbc.dao.*;
import jdbc.pojo.Customer;


public class ServiceImplementation implements ServiceInterface {
	
	Customer refCustomer;
	Scanner refScanner;
	DAOInterface refDAO;
	
	@Override
	public void customerChoice() {
		
		var menuLoop=true;
		while(menuLoop==true) {
			
			System.out.println("1.Logout");
			System.out.println("Enter Choice");

			refScanner = new Scanner(System.in);
			var choice = refScanner.nextInt();
			switch (choice) {
			case 1:
				closeConnection();
				menuLoop=false;
				break;
				
			default:
				System.out.println("Option not found..");
				break;

			}
		}
		
	}

	@Override
	public void customerAuthentication() {
		
		refScanner = new Scanner(System.in);

		System.out.println("Login Menu");
		System.out.println(" ");
		System.out.println("Enter User name:");
		String userId = refScanner.next();
		System.out.println("Enter password:");
		String userPassword = refScanner.next();
		
		
		refCustomer = new Customer();
		refCustomer.setCustomerID(userId);;
		refCustomer.setCustomerPassword(userPassword);
		
		
		refDAO = new DAOImplementation();
		
		if (refDAO.userLogin(refCustomer)) //check user authecation from database
		{
			customerChoice();
		} 
		else 
		{
			closeConnection();
		}
		
	}

	@Override
	public void closeConnection() {
		
		refDAO = new DAOImplementation();
		refDAO.closeConnection(refCustomer);
	}

}
