package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.pojo.Customer;
import utility.DBUtility;

public class DAOImplementation implements DAOInterface {

	Connection refConnection=null;
	PreparedStatement refPreparedStatement =null;

	@Override
	public void closeConnection(Customer refCustomer) {

		refCustomer=null;
		try {
			refConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Closing Connection..");
			System.exit(0);
		}
	}

	@Override
	public boolean userLogin(Customer refCustomer) {

		try {
			refConnection = DBUtility.getConnection();

			String sqlQuery = "select customerPassword,customerID from customer where customerID=?";


			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			System.out.println();

			refPreparedStatement.setString(1, refCustomer.getCustomerID());
			ResultSet rs=refPreparedStatement.executeQuery();
			rs.next();

			if(rs.getString("customerID").equals(refCustomer.getCustomerID()) && rs.getString("customerPassword").equals(refCustomer.getCustomerPassword()))
			{
				System.out.println("Login Success!");
				return true;
			}


		} catch (SQLException e) {
			System.out.println("Login Failed");
		}

		return false;
	}
}


