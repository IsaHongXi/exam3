package jdbc.dao;

import jdbc.pojo.Customer;


public interface DAOInterface {

	void closeConnection(Customer refUser);
	boolean userLogin(Customer refUser);
}
